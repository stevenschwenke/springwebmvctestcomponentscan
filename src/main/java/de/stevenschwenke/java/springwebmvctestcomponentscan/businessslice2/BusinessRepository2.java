package de.stevenschwenke.java.springwebmvctestcomponentscan.businessslice2;


import org.springframework.data.jpa.repository.JpaRepository;

public interface BusinessRepository2 extends JpaRepository<SomeBean, Long> {
}
