package de.stevenschwenke.java.springwebmvctestcomponentscan.businessslice2;

import org.springframework.stereotype.Service;

@Service
public class BusinessService2 {

    private final BusinessRepository2 businessRepository2;

    public BusinessService2(BusinessRepository2 businessRepository2) {
        this.businessRepository2 = businessRepository2;
    }

    public String doBusinessStuff() {
        return "business 2";
    }
}
