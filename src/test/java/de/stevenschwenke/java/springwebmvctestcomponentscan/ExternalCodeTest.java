package de.stevenschwenke.java.springwebmvctestcomponentscan;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import some.generated.stuff.ExternalService;

import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest
public class ExternalCodeTest {

    @Autowired
    private ExternalService externalService;

    @Test
    void externalServiceCanBeFound() {
        assertNotNull(externalService);
    }
}
