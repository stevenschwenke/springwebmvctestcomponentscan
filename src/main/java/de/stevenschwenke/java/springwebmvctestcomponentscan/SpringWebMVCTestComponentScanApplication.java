package de.stevenschwenke.java.springwebmvctestcomponentscan;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(value = {"de.stevenschwenke.java.springwebmvctestcomponentscan",
        "some.generated.stuff"})
public class SpringWebMVCTestComponentScanApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringWebMVCTestComponentScanApplication.class, args);
    }

}
