package de.stevenschwenke.java.springwebmvctestcomponentscan.businessslice3;

import org.springframework.stereotype.Service;

@Service
public class BusinessService3 {

    public String doBusinessStuff() {
        return "business 3";
    }
}
