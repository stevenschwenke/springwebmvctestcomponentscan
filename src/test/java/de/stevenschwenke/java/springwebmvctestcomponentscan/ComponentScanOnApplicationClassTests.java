package de.stevenschwenke.java.springwebmvctestcomponentscan;

import de.stevenschwenke.java.springwebmvctestcomponentscan.businessslice1.BusinessController1;
import de.stevenschwenke.java.springwebmvctestcomponentscan.businessslice1.BusinessService1;
import de.stevenschwenke.java.springwebmvctestcomponentscan.businessslice2.BusinessService2;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.ApplicationContext;
import org.springframework.test.web.servlet.MockMvc;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(BusinessController1.class)
class ComponentScanOnApplicationClassTests {

    @Autowired
    private MockMvc mockMvc;

    // Mocking this bean here is OK because it is a dependency from the controller under test.
    @MockBean
    private BusinessService1 businessService1;

    // Mocking this bean here is NOT OK and should not be necessary because it is a dependency from a controller
    // not under test. Removing this mock here causes the application context to fail because this service has a
    // repository as a dependency. In WebMvcTests, only the controller-layer is loaded, not repositories. That causes
    // the application context to fail if this mock here is removed.
//    @MockBean
//    private BusinessService2 businessService2;

    @Test
    void sliceCorrupted() {

        // A call to an endpoint not targeted in this MVC-test should throw an error here because only the controller
        // targeted in the @WebMvcTest-annotation is constructed. Hence, the endpoint /business3/ does not exist,
        // causing the call to return 404.

        assertThrows(AssertionError.class, () ->
                this.mockMvc.perform(get("/business3/"))
                .andExpect(status().isOk())
                .andExpect(content().string("business 3")));
    }

    @Test
    void shouldBeGreenTest() throws Exception {

        // This test is supposed to be green but fails because a repository is loaded in a non-mocked bean from slice
        // 2, which would make it necessary to mock slice 2 (see above).

        when(businessService1.doBusinessStuff()).thenReturn("correct return value");

        this.mockMvc.perform(get("/business1/"))
                .andExpect(status().isOk())
                .andExpect(content().string("correct return value"));
    }
}