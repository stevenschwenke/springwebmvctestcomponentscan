package de.stevenschwenke.java.springwebmvctestcomponentscan.businessslice1;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

@RestController
@RequestMapping("/business1")
public class BusinessController1 {

    private final BusinessService1 businessService1;

    public BusinessController1(BusinessService1 businessService1) {
        this.businessService1 = businessService1;
    }

    @GetMapping("/")
    public ResponseEntity<String> doStuff() {
        return ResponseEntity.of(Optional.of(businessService1.doBusinessStuff()));
    }

}
