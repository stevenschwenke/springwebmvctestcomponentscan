package de.stevenschwenke.java.springwebmvctestcomponentscan.businessslice2;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class SomeBean {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

}
