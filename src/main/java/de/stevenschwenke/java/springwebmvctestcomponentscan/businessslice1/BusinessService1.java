package de.stevenschwenke.java.springwebmvctestcomponentscan.businessslice1;

import org.springframework.stereotype.Service;

@Service
public class BusinessService1 {

    public String doBusinessStuff() {
        return "business 1";
    }
}
