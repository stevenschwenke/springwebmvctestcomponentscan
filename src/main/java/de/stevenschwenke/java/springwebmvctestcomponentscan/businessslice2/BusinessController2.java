package de.stevenschwenke.java.springwebmvctestcomponentscan.businessslice2;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

@RestController
@RequestMapping("/business2")
public class BusinessController2 {

    private final BusinessService2 businessService2;

    public BusinessController2(BusinessService2 businessService2) {
        this.businessService2 = businessService2;
    }

    @GetMapping("/")
    public ResponseEntity<String> doStuff() {
        return ResponseEntity.of(Optional.of(businessService2.doBusinessStuff()));
    }

}
