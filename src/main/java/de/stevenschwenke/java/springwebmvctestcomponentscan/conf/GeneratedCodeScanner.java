package de.stevenschwenke.java.springwebmvctestcomponentscan.conf;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(value = {"some.generated.stuff"})
public class GeneratedCodeScanner {
}
