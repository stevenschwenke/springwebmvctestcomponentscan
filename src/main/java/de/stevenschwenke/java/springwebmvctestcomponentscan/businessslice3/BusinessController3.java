package de.stevenschwenke.java.springwebmvctestcomponentscan.businessslice3;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

@RestController
@RequestMapping("/business3")
public class BusinessController3 {

    private final BusinessService3 businessService3;

    public BusinessController3(BusinessService3 businessService3) {
        this.businessService3 = businessService3;
    }

    @GetMapping("/")
    public ResponseEntity<String> doStuff() {
        return ResponseEntity.of(Optional.of(businessService3.doBusinessStuff()));
    }

}
